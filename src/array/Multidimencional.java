package array;

public class Multidimencional {

	public static void main(String[] args) {

		int[][] arregloMultidimencional = { { 1, 2 }, { 3, 4 }, { 5, 6, 7 }, { 8, 9, 0 } };

		System.out.println(arregloMultidimencional[0][0]);
		System.out.println(arregloMultidimencional[0][1]);
		System.out.println(arregloMultidimencional[1][0]);
		System.out.println(arregloMultidimencional[1][1]);
		System.out.println(arregloMultidimencional[2][0]);
		System.out.println(arregloMultidimencional[2][1]);
		System.out.println(arregloMultidimencional[2][2]);
		System.out.println(arregloMultidimencional[3][0]);
		System.out.println(arregloMultidimencional[3][1]);
		System.out.println(arregloMultidimencional[3][2]);

		for (int fila = 0; fila < arregloMultidimencional.length; fila++) {

			for (int columna = 0; columna < arregloMultidimencional[fila].length; columna++) {
				System.out.print(arregloMultidimencional[fila][columna]);
			}
		}
	}
}
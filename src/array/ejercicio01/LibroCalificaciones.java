package array.ejercicio01;

public class LibroCalificaciones {

	private String nombreDelCurso;
	private int[][] calificaciones;

	public LibroCalificaciones(String nombreDelCurso, int[][] calificaciones) {

		this.nombreDelCurso = nombreDelCurso;
		this.calificaciones = calificaciones;
	}

	public String getNombreDelCurso() {
		return nombreDelCurso;
	}

	public void setNombreDelCurso(String nombreDelCurso) {
		this.nombreDelCurso = nombreDelCurso;
	}

	public int[][] getCalificaciones() {
		return calificaciones;
	}

	public void setCalificaciones(int[][] calificaciones) {
		this.calificaciones = calificaciones;
	}

	public void procesarCalificaciones() {

		imprimirCalificaiones();

		System.out.printf("%n%s %d%n%s %d", 
				"La calificacion mas baja en el libro de calificaciones es: ",
				obtenerMinima(), 
				"La calificacion mas alta en el libro de calificaciones es: ", 
				obtenerMaxima());

		System.out.println("");

		imprimirGraficoBarras();

	}

	public int obtenerMinima() {

		int calificacionBaja = calificaciones[0][0];

		for (int[] calificarEstudiante : calificaciones) {

			for (int calificacion : calificarEstudiante) {

				if (calificacion < calificacionBaja) {
					calificacionBaja = calificacion;
				}
			}
		}
		return calificacionBaja;
	}

	public int obtenerMaxima() {

		int calificacionAlta = calificaciones[0][0];

		for (int[] calificarEstudiante : calificaciones) {

			for (int calificacion : calificarEstudiante) {

				if (calificacion > calificacionAlta) {
					calificacionAlta = calificacion;
				}
			}
		}
		return calificacionAlta;
	}

	public void imprimirCalificaiones() {
		System.out.printf("Las calificaciones son: %n%n");

		System.out.print("            ");

		for (int prueba = 0; prueba < calificaciones[0].length; prueba++) {
			System.out.printf("Prueba %d ", prueba + 1);
		}

		System.out.println("Promedio");

		for (int estudiante = 0; estudiante < calificaciones.length; estudiante++) {
			System.out.printf("Estudiante %2d", estudiante + 1);

			for (int prueba : calificaciones[estudiante]) {
				System.out.printf("%8d", prueba);
			}

			double promedio = obtenerPromedio(calificaciones[estudiante]);
			System.out.printf("%9.2f%n", promedio);

		}
	}

	public double obtenerPromedio(int[] conjuntoCalificaiones) {
		int total = 0;

		for (int calificaion : conjuntoCalificaiones) {
			total += calificaion;
		}

		return (double) total / conjuntoCalificaiones.length;
	}

	public void imprimirGraficoBarras() {

		System.out.println("Distribucion de calificaiones en general");

		int[] frecuencia = new int[11];

		for (int[] calificarEstudiantes : calificaciones) {
			for (int calificaion : calificarEstudiantes) {
				++frecuencia[calificaion / 10];
			}
		}

		for (int cuenta = 0; cuenta < frecuencia.length; cuenta++) {

			if (cuenta == 10) {
				System.out.printf("%5d: ", 100);
			} else {
				System.out.printf("%02d-%02d: ", cuenta * 10, cuenta * 10 + 9);
			}

			for (int estrellas = 0; estrellas < frecuencia[cuenta]; estrellas++) {
				System.out.print("*");
			}

			System.out.println("");
		}
	}

}